import dao.BookDAO;
import dao.DatabaseHelper;
import model.Book;

import java.sql.SQLException;
import java.sql.Statement;

public class Run {

    public static void main(String[] args) throws SQLException {

        BookDAO bookDAO = new BookDAO();

        Statement stmt = DatabaseHelper.newStatement();
        stmt.executeUpdate("DROP TABLE IF EXISTS boughtbooks");
        stmt.executeUpdate("DROP TABLE IF EXISTS book");
        stmt.executeUpdate("DROP TABLE IF EXISTS client");

        stmt.executeUpdate("CREATE TABLE book (" +
                "id bigserial NOT NULL," +
                "author varchar(255)," +
                "title varchar(255)," +
                "CONSTRAINT book_pkey PRIMARY KEY (id))");

        stmt.executeUpdate("CREATE TABLE client (" +
                "id bigserial NOT NULL," +
                "firstname varchar(255)," +
                "lastname varchar(255)," +
                "gender varchar(1)," +
                "CONSTRAINT client_pkey PRIMARY KEY (id))");

        stmt.executeUpdate("CREATE TABLE boughtbooks (" +
                "buyers_id bigint NOT NULL," +
                "boughtbooks_id bigint NOT NULL," +
                "CONSTRAINT fk_buyers FOREIGN KEY (buyers_id) REFERENCES client (id)," +
                "CONSTRAINT fk_boughtbooks FOREIGN KEY (boughtbooks_id) REFERENCES book (id))");

        Book b = new Book();
        b.setTitle("titre1");
        b.setAuthor("author1");

        Book b2 = new Book();
        b2.setTitle("titre2");
        b2.setAuthor("author2");

        bookDAO.create(b);
        bookDAO.create(b2);

        System.out.println(b.getId());
        System.out.println(b2.getId());

        DatabaseHelper.close(stmt);

    }
}
