package dao;

import model.Book;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookDAO {

    public Book create(Book book) {
        try {
            String insertSQL = "INSERT INTO book(title, author) VALUES (?,?)";
            PreparedStatement stmt = DatabaseHelper.newPreparedStatement(insertSQL, true);
            stmt.setString(1, book.getTitle());
            stmt.setString(2, book.getAuthor());
            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            generatedKeys.next();
            long id = generatedKeys.getLong("id");

            book.setId(id);
            DatabaseHelper.close(stmt);
            return book;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Book update(Book book) {
        //TODO
        return book;
    }

    public void delete(Long bookId) {
        //TODO
    }
}
