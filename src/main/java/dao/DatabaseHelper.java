package dao;

import java.sql.*;

public class DatabaseHelper {

    static String url = "jdbc:postgresql://localhost:5432/jdbcsample";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, "postgres", "postgres");
    }

    public static Statement newStatement() {
        try {
            return getConnection().createStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static PreparedStatement newPreparedStatement(String sql, boolean autoGeneratedKeys) {
        try {
            return getConnection().prepareStatement(sql,
                    autoGeneratedKeys
                            ? Statement.RETURN_GENERATED_KEYS
                            : Statement.NO_GENERATED_KEYS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void close(Statement stmt) {
        try {
            stmt.close();
            stmt.getConnection().close();
        } catch (SQLException e) {
            System.err.println("Can not close statement or connexion");
        }
    }
}
